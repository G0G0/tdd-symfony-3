<?php
namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;

class BaseRestController extends FOSRestController
{
    protected function serialize($data, $format = 'json')
    {
        return $this->container->get('serializer')->serialize($data, $format);
    }
}
