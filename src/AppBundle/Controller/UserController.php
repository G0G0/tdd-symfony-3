<?php
namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Controller\BaseRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * @Rest\Prefix("api/v1/user")
 * @Rest\NamePrefix("api_v1_user_")
 * @Rest\RouteResource("User")
 * 
 */
class UserController extends BaseRestController
{
    /**
     * Get the list of users.
     * 
     * @return Users[]
     * 
     * @Rest\Get("/list", name="list_users")
     *
     * @ApiDoc()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('AppBundle:User')->findAll();

        if ($users === null) {
            $response = array('There is no users exist');
            $serializedResponse = $this->serialize($response);
            return new Response($serializedResponse, Response::HTTP_NOT_FOUND);
        }
        $serializedEntity = $this->serialize($users);
        return new Response($serializedEntity, Response::HTTP_OK);
    }
    
    /**
     * Finds and displays a user entity.
     *
     * @Rest\Get("/{id}", name="user_show")
     * 
     * @return User[]
     * 
     * @ApiDoc()
     */
    public function showAction(User $user)
    {
        $serializedEntity = $this->serialize($user);
        return new Response($serializedEntity, Response::HTTP_OK);
    }
}
