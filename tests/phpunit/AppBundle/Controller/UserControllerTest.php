<?php
namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;

class UserControllerTest extends WebTestCase
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testIndex()
    {
        $client = static::createClient();

        $client->request('GET', '/api/v1/user/list');

        $response = $client->getResponse();

        /**
         * first assert status code
         */
        $this->assertSame(200, $response->getStatusCode());

        $responseData = json_decode($response->getContent(), true);

        /**
         * assert that data has all required keys
         */
        foreach ($responseData as $item) {
            $this->assertArrayHasKey('username', $item);
            $this->assertArrayHasKey('username_canonical', $item);
            $this->assertArrayHasKey('email', $item);
            $this->assertArrayHasKey('enabled', $item);
            $this->assertArrayHasKey('password', $item);
        }
    }

    public function testShow()
    {
        $users = $this->em
            ->getRepository('AppBundle:User')
            ->findAll();

        /**
         * get random user on every test
         */
        $user = $users[rand(0, count($users) - 1)];

        $client = static::createClient();

        $client->request('GET', '/api/v1/user/' . $user->getId());

        $response = $client->getResponse();

        $this->assertSame(200, $response->getStatusCode());

        $responseData = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('username', $responseData);
        $this->assertArrayHasKey('username_canonical', $responseData);
        $this->assertArrayHasKey('email', $responseData);
        $this->assertArrayHasKey('enabled', $responseData);
        $this->assertArrayHasKey('password', $responseData);
    }
    /**
     * It might be a good idea to separate tests in two folders: positive and negative tests
     * 
     * more test examples- http://symfony.com/doc/current/testing/doctrine.html
     * 
     * for post https://knpuniversity.com/screencast/rest/testing-phpunit and
     * 
     * https://ole.michelsen.dk/blog/testing-your-api-with-phpunit.html
     */
}
